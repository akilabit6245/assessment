﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment
{
    internal class Canvass
    {
        private Graphics graphics;
       
   /*     public Canvass(Graphics graphics)
        {
            this.graphics = graphics;
        }*/
        const int XSIZE = 640;
        const int YSIZE = 480;

        protected Color background_colour = Color.Gray;

        bool TESTING = false;
        Bitmap bm;
        Graphics g, cursorG;
        int XCanvasSize, YCanvasSize;
        Pen Pen;
        Color penColour;
        public int xpos, ypos;

        /// <summary>
        /// blank constructor
        /// </summary>
        public Canvass()
        {
            XCanvasSize = XSIZE;
            YCanvasSize = YSIZE;
            TESTING = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="cursorG"></param>
        public Canvass(Graphics g)//, Graphics cursorG)
        {
            this.g = g;
          //  this.cursorG = cursorG;
            XCanvasSize = XSIZE;
            YCanvasSize = YSIZE;
            xpos = ypos = 0;
            Pen = new Pen(Color.Black, 1);
        }

        /// <summary>
        /// initial constructor
        /// </summary>
        /// <param name="g"></param>
        /// <param name="xsize"></param>
        /// <param name="ysize"></param>
        public Canvass(Graphics g, int xsize, int ysize)
        {
            this.g = g;
            XCanvasSize = XSIZE;
            YCanvasSize = YSIZE;
            xpos = ypos = 0;
            Pen = new Pen(Color.Black, 1);
        }

        /// <summary>
        /// xpos property return
        /// </summary>
        public int Xpos
        {
            get
            {
                return xpos;
            }
        }

        public int Ypos
        {
            get
            {
                return ypos;
            }
        }

      /*  public void SetColour(int red, int green, int blue)
        {
            if (red > 255 || green > 255 || blue > 255)
                //throw new GPLexcetion("invalid colour " + red + "," + green + "," + blue);
            penColour = Color.FromArgb(red, green, blue);
            Pen = new Pen(penColour);

        }*/
        public void DrawLine(int toX, int toY) 
        {
            g.DrawLine(Pen, xpos, ypos, toX, toY);
            xpos=toX; 
            ypos=toY; 
        }
       /* public void DrawTo(int toX, int toY)
        {
            if (toX < 0 || toX > XCanvasSize || toY < 0 || toY > YCanvasSize)
               // throw new ApplicationException("invalid position");
            g.DrawLine(Pen, xpos, ypos, toX, toY);
            xpos = toX;
            ypos = toY;
            updateCursor();
        }*/
        /// <summary>
        /// clear the canvas
        /// </summary>
        public void Clear()
        {
            g.Clear(background_colour);
        }
        /// <summary>
        /// Draw circle
        /// </summary>
        /// <param name="radius"></param>
     /*   public void Circle(int radius)
        {
            if (radius < 0)
                throw new ApplicationException("\ninvalid circle size");
            g.DrawEllipse(Pen, xpos - radius, ypos - radius, radius * 2, radius * 2);

        }
        public void Rect(int width, int height)
        {
            if (width < 0 || height < 0)
                throw new ApplicationException("\ninvalid Rect size");
            g.DrawRectangle(Pen, xpos - (width / 2), ypos - (height / 2), width, height);
        }
        public void Tri(int width, int height)
        {
            if (width < 0 || height < 0)
                throw new ApplicationException("\n invalid Triangle size");
            g.DrawRectangle(Pen, xpos - (width / 2), ypos - (height / 2), width, height);
        }
        public void updateCursor() 
        {
            cursorG.Clear(Color.Transparent);
            Pen p = new Pen(Color.Black, 1);
            cursorG.DrawRectangle(p, xpos - 2, ypos - 2, 4, 4);
        }*/

    }
}



    

